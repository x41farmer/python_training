# python training

This project is a repository for use in learning python in a way that centers around practical applications. The methodology used in the Labs is taken from or insipred by several existing offensive C2 frameworks that use python such as Empire, Pupy, and others. The lecture material is tailored for time constraints but leverages the recently unclassified COMP3321, the MIT Open-Courseware and other free Python materials.

## Lecture

* Each major lesson contains an associated folder which contains enumerated objectives. It also contains the portions of the Lab you should be able to accomplish with this knowledge once complete
* Each lesson is in markdown format but will be parsed for presentation during class
* In addition to standard informational content. Lecture folders contain inline code blocks and 'work along' type code that students can use to further explore the conceps discussed in class.

## Labs

* The primary Lab of concern is the COOKIEMONSTER command post. This is used to assist in directed student learning by having students complete certain task handlers that are best aligned with the course material covered in lectures. 
* Please see the Labs folder for more detailed introduction and instruction to the COOKIEMONSTER command post.

## Disclaimer

This code and lecture material is UNCLASSIFIED and draws inspiration from several unclassified C2 frameworks and python learning materials. It should not be used to accomplish any objective other than to learn python fundamentals.