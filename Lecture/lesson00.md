# Lesson 0: Course Into OR What actually is Python, anyway?

## Intro
In this lesson you will be quickly exposed to Python as a concept and hopefully understand a bit more why you are expected to know it!

## Requirements
 - Experience in at least one other programming language
 - Access to a virtual machine that has python3

## Lesson

### Why Python
Python is a clear and powerful object-oriented programming language, comparable to Perl, Ruby, Scheme, or Java.

Some of Python's notable features:

 - Uses an elegant syntax, making the programs you write easier to read.
 - Is an easy-to-use language that makes it simple to get your program working. This makes Python ideal for prototype development and other ad-hoc programming tasks, without compromising maintainability.
 - Comes with a large standard library that supports many common programming tasks such as connecting to web servers, searching text with regular expressions, reading and modifying files.
 - Python's interactive mode makes it easy to test short snippets of code. There's also a bundled development environment called IDLE.
 - Is easily extended by adding new modules implemented in a compiled language such as C or C++.
 - Can also be embedded into an application to provide a programmable interface.
 - Runs anywhere, including Mac OS X, Windows, Linux, and Unix, with unofficial builds also available for Android and iOS.

Some programming-language features of Python are:

 - A variety of basic data types are available: numbers (floating point, complex, and unlimited-length long integers), strings (both ASCII and Unicode), lists, and dictionaries.
 - Python supports object-oriented programming with classes and multiple inheritances.
 - Code can be grouped into modules and packages.
 - The language supports raising and catching exceptions, resulting in cleaner error handling.
 - Data types are strongly and dynamically typed. Mixing incompatible types (e.g. attempting to add a string and a number) causes an exception to be raised, so errors are caught sooner.
 - Python contains advanced programming features such as generators and list comprehensions.
 - Python's automatic memory management frees you from having to manually allocate and free memory in your code.

### Philosophy of Python
Python was conceived in the late 1980s and its implementation was started in December 1989 by Guido van Rossum at CWI in Amsterdam. Python is a successor to the ABC programming language (itself inspired by SETL) capable of exception handling and interfacing with the Amoeba operating system. Van Rossum is Python's principal author, and his continuing central role in deciding the direction of Python is reflected in the title given to him by the Python community, Benevolent Dictator for Life (BDFL).

Python is a multi-paradigm programming language. Rather than forcing programmers to adopt a particular style of programming, it permits several styles: object-oriented programming and structured programming are fully supported, and there are a number of language features which support functional programming and aspect-oriented programming (including metaprogramming and "by magic" methods). Many other paradigms are supported using extensions, such as pyDBC and Contracts for Python which allow Design by Contract.

Rather than requiring all desired functionality to be built into the language's core, Python was designed to be highly extensible. New built-in modules can be easily written in C, C++ or Cython. Python can also be used as an extension language for existing modules and applications that need a programmable interface. This design, a small core language with a large standard library with an easily extensible interpreter, was intended by Van Rossum from the very start because of his frustrations with ABC (which espoused the opposite mindset).

### Lab Environment Setup
In this lab we will be setting up a Python3 virtual environment using the venv module (don't worry quite yet if this is confusing, think of modules as self contained python libraries that allow increased functionality). This method is a common way to standardize python environments across teams or projects.

The venv module provides support for creating lightweight “virtual environments” with their own site directories, optionally isolated from system site directories. Each virtual environment has its own Python binary (which matches the version of the binary that was used to create this environment) and can have its own independent set of installed Python packages in its site directories.

See PEP 405 for more information about Python virtual environments.

```bash
sudo yum install python3-pip
sudo pip install wheel venv
python3 -m venv ./env 
bash ./env/scripts/activate.sh
python3
```

You are now in a python environment that can be used to evaluate functions. There are other ways to interract with the python runtime envrionment but we will get to those soon. By convention, lines that begins with the '>>>' symbol is considered input and lines that do not are output.

```python
>>> print("Hello World!")
Hello World!
>>> 40 + 2
42
>>> print(4.0)
4.0
```

To exit, use the ```exit()``` function on all systems or \<CTRL\>-d on Linux.
```python
>>> exit()
```

## Resources
 - https://wiki.python.org/moin/BeginnersGuide/Overview
 - https://docs.python.org/3/library/venv.html
 - https://www.python.org/dev/peps/pep-0405/