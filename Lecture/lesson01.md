# Lesson 1: Basic DataTypes

## Intro
In this lesson we will learn some of the basic Python datatypes. The fundamental building blocks that make up this language.

## Requirements
 - functional Python3 virtual environment

## Lesson 

### Datatypes
#### Integers
The size of an integer is only constrained by the available memory of the system it is evaluated on. 
```python
>>> print(9999999999999999999999999999999999999999999999999990 + 1)
9999999999999999999999999999999999999999999999999991
```
By default, integers are evaluated as decimals if a prefix is not given. Some examples include:

 - Binary
     - 0b (zero + lowercase letter 'b')
     - 0B (zero + uppercase letter 'B')
 - Octal
     - 0o (zero + lowercase letter 'o')
     - 0O (zero + uppercase letter 'O')
 - Hexadecimal
     - 0x (zero + lowercase letter 'x')
     - 0X (zero + uppercase letter 'X')

The type() command returns the data type of the input object (we will get to objects). Each of the below return an int datatype because python uses ints to underly each of these numbers, no matter what way was used to specify them.

```python
>>> type(42)
<class 'int'>
>>> type(0o42)
<class 'int'>
>>> type(0x42)
<class 'int'>
```

#### Floats
Floating point numbers are specified with decimal points. The letter e followed by a positive or negative integer is used to specify scientific notation.

```python
>>> print(4.0)
4.2
>>> type(4.2)
<class 'float'>
>>>4.
4.0
>>> .2
0.2
>>> .4e7
4000000.0
>>> type(.4e7)
<class 'float'>
```

Almost all platforms represent Python float values as 64-bit “double-precision” values, according to the IEEE 754 standard. In that case, the maximum value a floating-point number can have is approximately 1.8 ⨉ 10308. Python will indicate a number greater than that by the string inf:

```python
>>> 1.79e308
1.79e+308
>>> 1.8e308
inf
```

The closest a nonzero number can be to zero is approximately 5.0 ⨉ 10-324. Anything closer to zero than that is effectively zero:

```python
>>> 5e-324
5e-324
>>> 1e-325
0.0
```

Floating point numbers are represented internally as binary (base-2) fractions. Most decimal fractions cannot be represented exactly as binary fractions, so in most cases the internal representation of a floating-point number is an approximation of the actual value. 

#### Strings
Strings are sequences of character data. The string type in Python is called str.

String literals may be delimited using either single or double quotes. All the characters between the opening delimiter and matching closing delimiter are part of the string:

```python
>>> print("I am a string.")
I am a string.
>>> type("I am a string.")
<class 'str'>

>>> print('I am too.')
I am too.
>>> type('I am too.')
<class 'str'>
```

A string can be as long as the memory on the system allows. It can also be empty.

To have a character literal in a string that is the same as the string delimiter special considerations must be made. i.e.:

```python
>>> print('this string ain't gonna hunt')
SyntaxError: invalid syntax
```

In this case a special escape character must be used that the interpreter recognizes and can adapt how the string is parsed. You can accomplish this with the backslash (\) character. This is an 'escape' character an supresses the normal interpretation of the character.

```python
>>> print('Strings are like universe\'s fundamental building block, man')
Strings are like universe's fundamental building block, man
```

The same thing works for double quotes as well

In addition to quotations, newline character ('\n') can also be escaped. This allows multi-line strings that do not contain whitespace.

```python
>>> print('a\
... b\
... c')
abc
```

**Note:** Python has no ```char``` or any type specific to one character, only ```str```s of length 1.

#### Boolean
Python booleans may be one of two values. True, or False

```python
>>> type(True)
<class 'bool'>
>>> type(False)
<class 'bool'>
```

In many cases, expressions in Python are evaluated in a boolean context. i.e. The expression is expected to either return a True or False.

### Variables
Variables in python may have arbitrarily long names and contain both letters and numbers but the first character must be a letter. Although legal to start a variable with an uppercase letter it is frowned upon and your tech director will hate you.

Python is a strongly typed language. That may confuse some when first learning how Python manages variables. In other languages, a variable declaration may include a specific type and this assignment cannot change for the life of the variable. In Python however, every variable represents a transparent object pointer that evaluates as if it is the object it has been assigned. When you assingn a new value to a variable, you essentially update the pointer in the variable to the value being assigned. This maintains type integrity but also a great deal of flexibility.

The best way to illustrate this is by example.

```python
>>> my_variable = 42
42
>>> type(my_variable)
<class 'int'>
>>> my_variable = "This a string and not the number 42"
"This a string and not the number 42"
>>> type(my_variable)
<class 'str'>
```

As is shown, the evaluation of 'my_variable' changes according to the type of object it points to. This flexibility allows dynamic variable assignment at runtime but can be quite dangerous if not handled properly. You may see modern Python written with explicit type checking but that is beyond what we are handling in this course.

### Lab

## Resources
 - https://realpython.com/python-data-types/