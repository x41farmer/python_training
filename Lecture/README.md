# python4cno Lectures (DRAFT)

### Lesson 0: Into OR What actually is Python, anyway?

### Lesson 1: Basic DataTypes 

### Lesson 2: Operators and Control Flow

### Lesson 3: Functions

### Lesson 4: Basic library usage & modules

### Lesson 5: Help, PDB and Object Introspection

### Lesson 6: Python 2 vs 3

### Lesson 7: Classes

### Lesson 8: Revisit modules and libraries

### Lesson 9: Generators & Comprehensions

### Lesson 10: Namespace, file operations

### Lesson 11: Error Handling

### Lesson 12: Structured data parsing (json / xml)

### Lesson 13: Encoding

### Lesson 14: Converting datatypes (ctypes, struct)

### Lesson 15: Pattern matching w/ regex

### Lesson 16: Process manipulation

### Lesson 17: Argparse & python cli env

### Lesson 18: Scanning & fuzzing

### Lesson 19: Web page serialization

### Lesson 20: Closures & decorators
