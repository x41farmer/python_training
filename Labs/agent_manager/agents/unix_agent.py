import requests
import time
import base64
import io
from uuid import uuid4
from PIL import Image

from agents.agent import BasicAgent
from models.command import CommandKey
from models.types import AgentType

class UnixAgent(BasicAgent):
    def __init__(self, config):
        super().__init__(config)
        self.implant_type = AgentType.GENERIC_UNIX
        self.response_folder = 'agent_manager/responses/ubuntu_1/'
        self.files_folder = 'agent_manager/responses/files/'
        self.process_id = 1316
        self.hostname = 'ubuntu2004.localdomain'

    def get_hostname(self):
        return self.hostname

    def get_operating_system(self):
        with open(self.response_folder + 'os_release', 'r') as f:
            response = f.read()
        return response

    def get_process_id(self):
        return str(self.process_id)

    def get_usernames(self):
        with open(self.response_folder + 'passwd', 'r') as f:
            response = f.read()
        return response

    def get_memory_usage(self):
        return ['3189', '4323', '1925', '1592', '2358']

    def get_directory_walk(self):
        with open(self.response_folder + 'dirwalk', 'rb') as f:
            data = f.read()
        return base64.b64encode(data)

    def get_kernel_info(self):
        with open(self.response_folder + 'uname_a', 'r') as f:
            response = f.read()
        return response

    def get_image_enhance(self):
        img = Image.open(self.files_folder + 'waltercronkite-250x313.jpg', mode='r')
        imgByteArr = io.BytesIO()
        img.save(imgByteArr, format='JPEG')
        return base64.b64encode(imgByteArr.getvalue())

    def get_export_file(self):
        with open(self.files_folder + 'ascii_spock', 'rb') as f:
            data = f.read()
        return base64.b64encode(data)
