# python4cno Implant

This implant is used to communicate with student's implementations of their command post servers. It will callback every couple of seconds to see if there is a new task.

### Tests

Run tests with pytest, at this point there is basic coverage of the command post server and additional test are required for the tasking parsers