from agents.unix_agent import UnixAgent
from config import config

if __name__ == "__main__":
    agent = UnixAgent(config)
    agent.run()