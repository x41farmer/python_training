# python4cno Labs

## Basic Startup

* To setup Command Post Database
```bash
bash reset_db.sh
```

* To Run the Command Post
```bash
bash start_server.sh
```

* To Run the default Implant
```bash
cd Labs/implant
python3 run.py
```

## Student Lab Instructions

* Each Lesson has specific coresponding task parsers to implement in the COOKIEMONSTER Command Post. These are located in `Labs/command_post/task_parsers/*.py` where `*` is the coresponding task name and all are available as skeletons already in addition to task parser template files that may be expanded. For specific implementation instructions see the documentation in the skeleton parser file.

### Student FAQ

* This looks really complicated, I'm a bit overwhelmed
    * Don't worry! We are only asking you fill out some template functions. It is perfectly OK to ignore the rest of what is going on with the Command Post for now
* What is a command post?
    * Typically, command posts are used to control or recover information from one or more implants through callback mechanisms that can be incredibly diverse. These may or may not be written in python.
* How does the command post work even?
    * This server replicates 'poorly' a system where an implant may receive tasks or 'instructions' to execute on behalf of the command post. These results are sent back to the server for further parsing
    * This service primarily uses Flask, a python module that parses and routes incoming web requests to targets that we use to manage the Command Post <-> implant communications
    * The COOKIEMONSTER command post specifically uses a not-so secretive method of communicating with implants using cookie fields in web requests and results
* I looked through your code and it is a mess
    * We appreciate any assistance in ensuring the code that students see is clean, readible, and correct. Please point out any issues you find.
* Why are we filling out functions in this way?
    * By isolating the student's code into separate modules we can allow students to parse and return data without worrying about all the network and communications underpinning the server.



## Content Development Instructions

* Current focus is on lesson content and task parsers that Students will be replicating. For now, working implementations and documentation is all that is necessary. 

* Prior to starting the class, we will mask the solution implementations and have testers go through the course materials to assess instruction shortfalls

* To add a new task, copy the `template_parser.py` file in `Labs/command_post/task_parsers/`. The run() function will be called when this parser is tasked and all checkin data has been returned from the implant.
    * Copy `Labs/command_post/task_parsers/template_parser.py`
    * Update/Ensure `Labs/command_post/task_parsers/__init__.py` `parser_map` contains your task parser mapped to the task parser ID. 
    * Update `Labs/implant/implant.py` to contain your task parser ID as an option in the `CommandKey(Enum)`.
    * Update `Labs/implant/implant.py` to contain what the implant will return as results. Although we don't yet have different implant types, we may in the future.
    * Complete your task parser implementation
    * Test w/ the tests TODO: make tests