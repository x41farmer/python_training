import pytest

from command_post.db import get_db

def test_index(client):
    response = client.get("/")
    assert b'Agent: test' in response.data
    assert b'/tasks/1/create' in response.data