import pytest

from command_post.db import get_db

def test_index(client):
    response = client.get("/tasks/")
    assert b'Task: 523338e9-c917-469d-90ba-df17d3fefa4d' in response.data
    assert b'Command: 1 Params:' in response.data

def test_tasks_by_agent(client):
    response = client.get("/tasks/1")
    assert b'test Tasks' in response.data
    assert b'Task: 523338e9-c917-469d-90ba-df17d3fefa4d' in response.data

def test_non_existant_agent_tasks(client):
    response = client.get("/tasks/0")
    assert b"Agent id 0 doesn't exist" in response.data
    response = client.get("/tasks/0/create")
    assert b"Agent id 0 doesn't exist" in response.data

def test_create_task(client, app):
    assert client.get('/tasks/1/create').status_code == 200
    client.post('/tasks/1/create', data={"command_id": "1", "command_params": ""})

    with app.app_context():
        db = get_db()
        count = db.execute("SELECT COUNT(id) FROM task").fetchone()[0]
        assert count == 2