import pytest
from flask import g
from flask import session
from uuid import uuid4

from werkzeug.security import check_password_hash, generate_password_hash

from command_post.db import get_db

def test_register(client, app):
    # test that viewing the page redirects to a 404 if not registered
    assert client.get("/register").status_code == 404

    agent_uuid = str(uuid4())
    client.set_cookie('localhost', key='uuid', value=agent_uuid)
    client.set_cookie('localhost', key='password', value=str(uuid4()))
    client.set_cookie('localhost', key='preshared_key', value='COOKIEMONSTER')
    # test that successful registration redirects to the login page
    response = client.get("/register")

    #Test that we get a redirect to the success page
    assert 302 == response.status_code

    # test that the user was inserted into the database
    with app.app_context():
        assert (
            get_db().execute("select * from agent where agent_uuid = ?", (agent_uuid,)).fetchone()
            is not None
        )

def test_fail_checkin(client):
    assert client.get("/cgi-bin/checkin").status_code == 404

def test_checkin(auth_client, app):
    response = auth_client.get("/cgi-bin/checkin")
    with app.app_context():
        db = get_db()
        count = db.execute("SELECT COUNT(id) FROM checkin").fetchone()[0]
        assert count == 2
    assert response.status_code == 200

def test_receive_task(auth_client, app):
    assert auth_client.get("/cgi-bin/checkin").status_code == 200
    task_uuid = None
    for cookie in auth_client.cookie_jar:
        if cookie.name == 'task_uuid':
            task_uuid = cookie.value
    assert(task_uuid == '523338e9-c917-469d-90ba-df17d3fefa4d')

    # test that the task was updated as 'issued' in the database
    with app.app_context():
        assert (
            get_db().execute("select issued from task where task_uuid = ?", (task_uuid,)).fetchone()['issued']
            is not None
        )

def test_complete_task(auth_client, app):
    #Get the task assigned
    assert auth_client.get("/cgi-bin/checkin").status_code == 200
    task_uuid = None
    for cookie in auth_client.cookie_jar:
        if cookie.name == 'task_uuid':
            task_uuid = cookie.value
    assert(task_uuid == '523338e9-c917-469d-90ba-df17d3fefa4d')
    with app.app_context():
        assert (
            get_db().execute("select issued from task where task_uuid = ?", (task_uuid,)).fetchone()['issued']
            is not None
        )

    auth_client.set_cookie('localhost', 'checkin_data', 'completed_test')
    auth_client.set_cookie('localhost', 'task_status', '1')
    assert auth_client.get("/cgi-bin/checkin").status_code == 200
    
    # test that the task was updated as 'completed' in the database with correct results
    with app.app_context():
        task_data = get_db().execute("select completed, results from task where task_uuid = ?", (task_uuid,)).fetchone()
        assert (task_data['completed'] is not None)
        assert (task_data['results'] == 'completed_test')