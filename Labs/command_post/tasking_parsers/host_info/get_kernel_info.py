"""
@file: get_kernel_info.py
COMMAND_ID = 3

Parse and return kernel information given string output of uname -a command
or equivalent to the following specification:

examples:
    'Linux DESKTOP-7QUPOEN 4.19.104-microsoft-standard #1 SMP Wed Feb 19 06:37:35 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux' -> 'arch: x86_64, release: 4.19.104-microsoft-standard'


"""

def describe():
    return "Get the kernel info of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return the kernel info given the raw user agent string
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: String
    """
    print("Running 'Get kernel info'")
    parsed_results = raw_results.pop()
    print("Got kernel info: {0}".format(parsed_results))
    return parsed_results