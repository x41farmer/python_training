"""
@file: get_process_id.py
COMMAND_ID = 4

Receive a string representation of a base 10 value and return the pid of the 
process executing the implant as a string representation of a base 16 integer 
(hex) 

examples:
    '1316' -> '0x524'

"""

def describe():
    return "Get the hostname of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return the hostname given the raw user agent string
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: int
    """
    #Setup Section
    print("Running 'Get Process ID'")
    single_result = raw_results.pop()
    parsed_results = None

    #Student Section [FILL THIS IN]

    result_as_integer = int(single_result)
    parsed_results = hex(result_as_integer)

    #End Student Section

    #Return parsed results
    print("Got Hostname: {0}".format(parsed_results))
    return parsed_results