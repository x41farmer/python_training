"""
@file: get_operating_system.py
COMMAND_ID = 2

Parse the operating system information out of a multi-line list of 
attributes and return a string with the following spec:

operating_system:major version:minor version

examples:
    ubuntu:20:04
    windows:10:0
"""

def describe():
    return "Get the operating system of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return the operating system given the raw user agent string
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: String
    """
    print("Running 'Get Operating System'")
    parsed_results = raw_results.pop()
    print("Got Operating System: {0}".format(parsed_results))
    return parsed_results