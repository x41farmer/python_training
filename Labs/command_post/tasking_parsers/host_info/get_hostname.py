"""
@file: get_hostname.py
COMMAND_ID = 1

Return the hostname of the device given raw string input that either contains
the device's fully qualified domain name or only the hostname

examples:
    ubuntu2004.localdomain -> ubuntu2004
    DESKTOP-XV7384 -> DESKTOP-XV7384

"""

def describe():
    return "Get the hostname of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return the hostname given the raw user agent string
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: String
    """
    print("Running 'Get Hostname'")
    parsed_results = raw_results.pop()
    print("Got Hostname: {0}".format(parsed_results))
    return parsed_results