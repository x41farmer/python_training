"""
@file: get_memory_usage.py
COMMAND_ID = 5

Receive a list of memory usage values and return their average in megabytes

examples:
    [3189, 4323, 1925, 1592, 2358] -> '2677.4'

"""

def describe():
    return "Get the hostname of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return the hostname given the raw user agent string
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: int
    """
    #Setup Section
    print("Running 'Get Average Memory Usage'")
    memory_usage_list = list(map(int, raw_results))
    parsed_results = None

    #Student Section [FILL THIS IN]

    result_as_float = sum(memory_usage_list)/len(memory_usage_list)
    parsed_results = str(result_as_float)

    #End Student Section [REMEMBER THE RETURN VALUE MUST BE A STRING]

    #Return parsed results
    print("Got Hostname: {0}".format(parsed_results))
    return parsed_results