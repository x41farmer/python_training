import base64

def combine_and_decode(segmented_base64_data):
    #STUDENTS FILL THIS IN
    return base64.b64decode(''.join(segmented_base64_data).encode())
    #END OF STUDENT SECTION
    return None