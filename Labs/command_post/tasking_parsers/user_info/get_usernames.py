"""
@file: get_users.py
COMMAND_ID = 6

Parse and return a list of the usernames of accounts on the device in a colon
delimited string

examples:
    contents of /etc/passwd -> 'root:user:daemon:bin'


"""

def describe():
    return "Get the usernames of the accounts on the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return a list of the usernames of accounts on the device
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: colon delimited string of usernames
    """
    #Setup Section
    print("Running 'Get usernames'")
    raw_etc_passwd = raw_results.pop()
    parsed_results = ''

    #Student Section [FILL THIS IN]

    users = []
    raw_users_list = raw_etc_passwd.split()
    for raw_user_line in raw_users_list:
        user_line_as_list = raw_user_line.split(':')
        username = user_line_as_list[0]
        users.append(username)

    for user in users:
        parsed_results = parsed_results + ':' + user

    #remove leading colon
    if parsed_results[0] == ':':
        parsed_results = parsed_results[1:]

    #Alternate method
    #users = [user_line.split(':')[0] for user_line in raw_etc_passwd.split()]
    #parsed_results = ':'.join(users)

    #End Student Section

    #Return parsed results
    print("Got usernames: {0}".format(parsed_results))
    return parsed_results