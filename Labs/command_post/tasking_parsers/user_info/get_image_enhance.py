"""
@file: get_image_tags.py
COMMAND_ID = 8

Receive base64 encoded jpg image data as an Image.array and return an image
with the contrast increased by 30%, ENHANCE!

"""

import io
import base64
from PIL import Image
from PIL import ImageEnhance
from command_post.tasking_parsers.utils import combine_and_decode

def describe():
    return "Get and enhance an image\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse a base64 encoded jpeg and return the same image with 30% more contrast
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: int
    """
    #Setup Section
    print("Running 'Get Enhanced Image'")
    raw_base64_image_data = raw_results
    parsed_results = ''
    # open image file from bytes like so: Image.open(io.BytesIO(raw_image_data))

    #Student Section [FILL THIS IN]
    raw_image_data = combine_and_decode(raw_base64_image_data)
    image = Image.open(io.BytesIO(raw_image_data))
    enh = ImageEnhance.Contrast(image)
    enhanced_image = enh.enhance(1.3)

    #End Student Section

    # Convert Image back to byte array
    imgByteArr = io.BytesIO()
    enhanced_image.save(imgByteArr, format='JPEG')
    parsed_results = imgByteArr.getvalue()

    #Return parsed results
    print("Got Enhanced Image: {0}".format(parsed_results))
    return parsed_results