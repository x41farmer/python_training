"""
@file: get_directory_walk.py
COMMAND_ID = 7

Receive seperate base64 encoded strings in a list that represent chunks of a directory walk
and return the decoded directory walk as a single string. 

Note: First combine the output then decode the total string

rough example:
    [base64_chunk, base64_chunk, base64_chunk] -> 'decoded directory walk'

"""

import base64

def describe():
    return "Get the directory walk of the device\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse and return a decoded directory walk of the users home directory
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: int
    """
    #Setup Section
    print("Running 'Get Directory walk'")
    raw_base64_results = raw_results
    parsed_results = ''

    #Student Section [FILL THIS IN]

    total_base64_result = ''

    # First combine the input
    for result in raw_base64_results:
        total_base64_result = total_base64_result + result

    #then decode the string
    parsed_results = base64.b64decode(total_base64_result)

    #End Student Section

    #Return parsed results
    print("Got Directories: {0}".format(parsed_results))
    return parsed_results