"""
@file: export_file.py
COMMAND_ID = 9

Receive and decode base64 encoded file and save it to the 
command_post/exports/ directory

savefile format:
    agent_uuid_%Y-%m-%d_%H:%M:%S

"""

import base64
from datetime import datetime
from command_post.tasking_parsers.utils import combine_and_decode

def describe():
    return "Get the exif tags from image\nParams: None"

def run(tasked_agent, raw_results, command_parameters):
    """Parse a base64ed file and save it to command_post/exports/ directory
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: String
    """
    #Setup Section
    print("Running 'Save Export File'")
    raw_base64_file_data = raw_results
    parsed_results = ''

    #Student Section [FILL THIS IN]
    raw_filedata = combine_and_decode(raw_base64_file_data)
    time_str = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    file_name = tasked_agent.agent_uuid + '_' + time_str
    with open('command_post/exports/' + file_name, 'w') as f:
        f.write(raw_filedata)

    #End Student Section

    #Return parsed results
    print("Got Enhanced Image: {0}".format(parsed_results))
    return parsed_results