
from command_post.tasking_parsers import template_parser
from command_post.tasking_parsers.host_info import (
    get_hostname, get_operating_system, get_kernel_info, get_process_id, 
    get_memory_usage,
    )
from command_post.tasking_parsers.user_info import (
    get_usernames, get_directory_walk, get_image_enhance,
)
from command_post.tasking_parsers.external import (
    get_export_file
)

#This object maps tasking parsers to their command Id
parser_map = {
    0: template_parser, 
    1: get_hostname,
    2: get_operating_system,
    3: get_kernel_info,
    4: get_process_id,
    5: get_memory_usage,
    6: get_usernames,
    7: get_directory_walk,
    8: get_image_enhance,
    9: get_export_file,
    }

def get_parser(cmd_id):
    return parser_map.get(cmd_id)

