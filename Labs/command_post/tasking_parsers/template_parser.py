#COMMAND_ID = 0

def describe():
    return "This is a template command handler"

def run(tasked_agent, raw_results, command_parameters):
    """This template shows how results are parsed by the command post
    
    Keyword arguments:
    tasked_agent - the Agent() object that has returned this task data
    raw_results - a List of all raw results returned by agent for this task
    command_parameters - a raw string of parameters used by agent for this task

    Return: String
    """
    print("Running 'Template Handler'")
    return ''.join(raw_results)