from enum import Enum

class CommandKey(Enum):
    GET_HOSTNAME = 1
    GET_OPERATING_SYSTEM = 2
    GET_KERNEL_INFO = 3
    GET_PROCESS_ID = 4
    GET_MEMORY_USAGE = 5
    GET_USERNAMES = 6
    GET_DIRECTORY_WALK = 7
    GET_IMAGE_ENHANCE = 8
    GET_EXPORT_FILE = 9

class CommandStatus(Enum):
    RUNNING = 0
    COMPLETE = 1
    GENERAL_FAILURE = 500
    NOT_SUPPORTED = 502