from werkzeug.security import check_password_hash, generate_password_hash

from command_post.db import get_db
from command_post.models.task import get_next_task, get_current_task
from command_post.models.command import CommandStatus

def get_agent(agent_uuid, password=None):
    agent_map = get_db().execute(
            'SELECT * FROM agent WHERE agent_uuid = ?', (agent_uuid,)
        ).fetchone()
    if agent_map:
        agent = Agent(agent_map)
        if password:
            if check_password_hash(agent.password_hash, password):
                return agent
            else:
                print("Failed agent password check {0}".format(password))
        else:
            return agent
    return None

def save_agent(agent_uuid, password):
    db = get_db()
    db.execute(
        'INSERT INTO agent (agent_uuid, password) VALUES (?, ?)',
        (agent_uuid, generate_password_hash(password))
    )
    db.commit()

class Agent():
    def __init__(self, agent_map):
        self.id = agent_map['id']
        self.uuid = agent_map['agent_uuid']
        self.password_hash = agent_map['password']
        self.current_task = get_current_task(self)

    def is_tasked(self):
        return True if self.current_task else False

    def get_next_task(self):
        self.current_task = get_next_task(self)
        return self.current_task

    def cancel_task(self):
        if self.is_tasked():
           self.current_task.cancel()
        self.current_task = None

    def run_task(self):
        if self.is_tasked():
            self.current_task.run()
        else:
            raise ValueError("No Task Found")
        self.current_task = None

    def checkin(self, data):
        db = get_db()
        db.execute(
            'INSERT INTO checkin (checkin_agent_id, current_task_id, checkin_data)'
            ' VALUES (?, ?, ?)',
            (self.id, self.current_task.id if self.is_tasked() else None, data)
        )
        db.commit()

    def update_task_status(self, status_code):
        if not status_code:
            return
        if type(status_code) == str:
            status_code = CommandStatus(int(status_code))
        if status_code == CommandStatus.COMPLETE:
            # Execute the task
            self.run_task()

        elif status_code == CommandStatus.GENERAL_FAILURE:
            print("Task Failed")
            self.cancel_task()
        elif status_code == CommandStatus.NOT_SUPPORTED:
            print("Task not supported")
            self.cancel_task()