from datetime import datetime

from command_post.tasking_parsers import get_parser
from command_post.db import get_db

def get_current_task(agent):
    current_task = get_db().execute(
            'SELECT * FROM task WHERE assigned_agent_id = ?'
            ' AND completed IS NULL AND issued IS NOT NULL '
            'ORDER BY created ASC', 
            (agent.id,)
        ).fetchone()
    return Task(current_task, agent) if current_task else None

def get_next_task(agent):
    next_task = get_db().execute(
            'SELECT * FROM task WHERE assigned_agent_id = ?'
            ' AND issued IS NULL ORDER BY created ASC', 
            (agent.id,)
        ).fetchone()
    return Task(next_task, agent) if next_task else None

class Task:
    def __init__(self, task_map, tasked_agent):
        if not task_map:
            self.is_valid = False
        else:
            #TODO: improve task validation on input
            self.is_valid = True
        self.id = task_map['id']
        self.uuid = task_map['task_uuid']
        self.assigned_agent = task_map['assigned_agent_id']
        self.command_id = task_map['command_id']
        self.command_params = task_map['command_params']
        self.tasked_agent = tasked_agent
        self.raw_results = []

    def __str__(self):
        return "id:{0} uuid:{1} assigned_agent_id:{2} command_id:{3} command_params:{4}".   format(self.id, self.uuid, self.assigned_agent, self.command_id, self.command_params)

    def complete(self, results=None):
        db = get_db()
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        db.execute('UPDATE task SET results = ?, completed = ?'
                'WHERE id = ?',
                (results, timestamp, self.id))
        db.commit()

    def cancel(self):
        print("Cancelling task {0}".format(self.uuid))
        if self.is_valid:
            self.complete()
        self.is_valid = False

    def run(self):
        error = None
        parser = get_parser(self.command_id)
        if not self.is_valid:
            error = 'Not a valid Task'
        elif not parser:
            error = 'No command parser found'
        else:
            db = get_db()
            raw_result_rows = db.execute(
                    'SELECT checkin_data FROM checkin '
                    'WHERE current_task_id = ?', (self.id,)
                ).fetchall()
            self.raw_results = [row['checkin_data'] for row in raw_result_rows]
            try:
                parsed_results = parser.run(
                    self.tasked_agent, 
                    self.raw_results, 
                    self.command_params)
                if not parsed_results: raise ValueError("Result must not be none")
                self.complete(parsed_results)
            except Exception as e:
                #TODO make this cleaner
                print('ERROR: {0}'.format(e))
                error = 'Task failed with {0}'.format(e)
                self.complete()

        return error