from enum import Enum

class AgentType(Enum):
    GENERIC_UNIX = 1
    GENERIC_WINDOWS = 2