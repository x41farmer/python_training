DROP TABLE IF EXISTS agent;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS checkin;

CREATE TABLE agent (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  agent_uuid TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE task (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  assigned_agent_id INTEGER NOT NULL,
  task_uuid TEXT UNIQUE NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  issued TIMESTAMP,
  completed TIMESTAMP,
  command_id INTEGER NOT NULL,
  command_params TEXT NOT NULL,
  results BLOB,
  FOREIGN KEY (assigned_agent_id) REFERENCES agent(id)
);

CREATE TABLE checkin (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  checkin_agent_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  current_task_id INTEGER,
  checkin_data BLOB,
  FOREIGN KEY (checkin_agent_id) REFERENCES agent(id),
  FOREIGN KEY (current_task_id) REFERENCES task(id)
);