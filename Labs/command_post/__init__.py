import os
from flask import (
    Flask, current_app, g, jsonify, request, redirect, url_for, 
    render_template, make_response)

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'command_post.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/success')
    def success():
        return 'Success!'

    from . import db
    db.init_app(app)

    from . import checkin
    app.register_blueprint(checkin.bp)

    from . import tasking
    app.register_blueprint(tasking.bp)

    from . import agents
    app.register_blueprint(agents.bp)
    app.add_url_rule('/', endpoint='index')

    return app