
from uuid import uuid4
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from command_post.db import get_db

bp = Blueprint('tasking', __name__,  url_prefix='/tasks')

@bp.route('/')
def index():
    db = get_db()
    tasks = db.execute(
        'SELECT id, command_id, command_params, results, created, issued, completed, task_uuid FROM task t '
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('tasking/index.html',
         tasks=tasks, agent=None)

@bp.route('/<int:id>')
def tasks_by_agent(id):
    agent = get_agent(id)
    db = get_db()
    tasks = db.execute(
        'SELECT id, command_id, command_params, results, created, issued, completed, task_uuid FROM task t '
        'WHERE assigned_agent_id is ? ORDER BY created DESC',
        (agent['id'],)
    ).fetchall()
    return render_template('tasking/index.html',
         tasks=tasks, 
         agent=agent)

@bp.route('/<int:id>/create', methods=('GET', 'POST'))
def create(id):
    agent = get_agent(id)
    if request.method == 'POST':
        command_id = request.form['command_id'] #TODO: make this cleaner
        command_params = request.form['command_params']
        assigned_agent_id = agent['id']
        error = None

        if not command_id:
            error = 'Command ID.'

        if not assigned_agent_id:
            error = 'Assigned Agent.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO task (command_id, command_params, assigned_agent_id, task_uuid)'
                ' VALUES (?, ?, ?, ?)',
                (command_id,
                 command_params,
                 assigned_agent_id, 
                str(uuid4()))
            )
            db.commit()
            return redirect(url_for('tasking.tasks_by_agent', id=agent['id']))

    return render_template('tasking/create.html')

def get_agent(id):
    agent = get_db().execute(
        'SELECT id, agent_uuid FROM agent WHERE id = ?',
        (id,)
    ).fetchone()

    if agent is None:
        abort(404, "Agent id {0} doesn't exist".format(id))

    return agent

def get_task(id):
    task = get_db().execute(
        'SELECT id, command_id, command_params, created, assigned_agent_id,'
        'issued, completed, results FROM task WHERE id = ?',
        (id,)
    ).fetchone()

    if task is None:
        abort(404, "Task id {0} doesn't exist.".format(id))

    return task

@bp.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    get_task(id)
    db = get_db()
    db.execute('DELETE FROM task WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('tasking.index'))