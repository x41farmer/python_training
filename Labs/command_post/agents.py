from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from command_post.db import get_db

bp = Blueprint('agents', __name__)

@bp.route('/')
def index():
    db = get_db()
    agents = db.execute(
        'SELECT a.id, agent_uuid, max(c.created) as last_checkin '
        ' FROM agent a JOIN checkin c ON '
        'a.id = c.checkin_agent_id GROUP BY agent_uuid '
        'ORDER BY created DESC'
    ).fetchall()
    return render_template('agents/index.html', agents=agents)