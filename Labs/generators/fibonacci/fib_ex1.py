#!/usr/bin/env python3
"""Find the first 20 fibonacci numbers using a for loop"""

a = 0
b = 1

for _ in range(20):
    a, b = b, a + b
    print(a, end=' ')
print()

