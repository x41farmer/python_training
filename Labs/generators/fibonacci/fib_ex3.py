#!/usr/bin/env python3
"""Find the first 20 fibonacci numbers using a class that is an iterator."""

import itertools

class Fib:
    def __init__(self):
        """Constructor, initialize the first values"""
        self.a = 0
        self.b = 1

    def __iter__(self):
        """Set this object to be the iterator for itself"""
        return self

    def __next__(self):
        """Specify what happends for next(Fib())"""
        self.a, self.b = self.b, self.a + self.b
        return self.a

for number in itertools.islice(Fib(), 20):
    print(number, end=' ')
print()

