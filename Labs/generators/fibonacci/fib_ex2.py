#!/usr/bin/env python3
"""Find the first 20 fibonacci numbers using a generator."""

import itertools

def fib():
    a = 0
    b = 1
    while True:
        a, b = b, a + b
        yield a

for number in itertools.islice(fib(), 20):
    print(number, end=' ')

print()

