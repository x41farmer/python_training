#!/usr/bin/env python3

"""
RC4 is a stream cipher which encrypts a message byte-by-byte using a Pseudo-random number generator algorithm (PRGA).
The PRGA is initialized by a key and the same key always makes the same stream.

Fill in the rc4 function with an RC4 implementation to find the plaintext.
Try to solve without looking at the solution!

Simple RC4 Algorithm based on https://en.wikipedia.org/wiki/RC4

Pseudocode below:

Key-scheduling algorithm (KSA)

for i from 0 to 255
    S[i] := i
endfor
j := 0
for i from 0 to 255
    j := (j + S[i] + key[i mod keylength]) mod 256
    swap values of S[i] and S[j]
endfor

Pseudo-random generation algorithm (PRGA)

i := 0
j := 0
while GeneratingOutput:
    i := (i + 1) mod 256
    j := (j + S[i]) mod 256
    swap values of S[i] and S[j]
    K := S[(S[i] + S[j]) mod 256]
    output K
endwhile

"""

def rc4(key):
    """
    rc4 generator
    1) Run Key-scheduling algorithm based on provided key.
    2) Yield individual bytes of the PRGA bytes
    """
    # 1) Write KSA Here

    # 2) Write PRGA Here

    while True:
        K = 0 # delete this line when you add your code!
        yield K

# No need to edit below

key = b'Secret'
ciphertext = b'\x45\xa0\x1f\x64\x5f\xc3\x5b\x38\x35\x52\x74\x4b\x9b\xf5'

# decrypt and print the plaintext
# zip() will interleave the two iterables into an iterable of tuples of (cipher_byte, rc4_byte)
for cipher_byte, rc4_byte in zip(ciphertext, rc4(key)):
    # xor to decrypt
    print(chr(cipher_byte ^ rc4_byte), end='')
print()

