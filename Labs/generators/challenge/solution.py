#!/usr/bin/env python3

"""
Possible Solution
"""

def rc4(key):
    """
    rc4 generator
    1) Run Key-scheduling algorithm based on provided key.
    2) Yield individual bytes of the PRGA bytes
    """
    # 1) Write KSA Here
    S = list(range(256))
    j = 0
    for i in range(256):
        j = (j + S[i] + key[i % len(key)]) % 256
        S[i], S[j] = S[j], S[i]

    # 2) Write PRGA Here
    i = 0
    j = 0
    while True:
        i = (i+1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]
        K = S[(S[i] + S[j]) % 256]
        yield K

# No need to edit below

key = b'Secret'
ciphertext = b'\x45\xa0\x1f\x64\x5f\xc3\x5b\x38\x35\x52\x74\x4b\x9b\xf5'

# decrypt and print the plaintext
# zip() will interleave the two iterables into an iterable of tuples of (cipher_byte, rc4_byte)
for cipher_byte, rc4_byte in zip(ciphertext, rc4(key)):
    # xor to decrypt
    print(chr(cipher_byte ^ rc4_byte), end='')
print()

