Instructor:

-------------------------------------------------------------------------------------------------------
1) After discussing generators, recommend having students interactively use next on the iter of a list like this:
```
>>> my_list = [1,2,3]
>>> my_list_iter = iter(my_list)
>>> my_list_iter
<list_iterator object at 0x7f7202154eb8>
>>> next(my_list_iter)
1
>>> next(my_list_iter)
2
>>> next(my_list_iter)
3
>>> next(my_list_iter)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```

Explain what iter does, next does, and why StopIteration is raised.

-------------------------------------------------------------------------------------------------------
2) Have students make this tiny generator

Describe each yield is like a temporary return until the next value is requested.

```
>>> def my_gen():
...     yield 1
...     yield 2
...     yield 3
```

Show this is just a function.
```
>>> my_gen
<function my_gen at 0x7f720214dbf8>
```

Until it is called and it become a generator.
```
>>> my_gen()
<generator object my_gen at 0x7f7202152f10>
```

Show this looks the same as a list the way we just used it:
```
>>> gener = my_gen()
>>> next(gener)
1
>>> next(gener)
2
>>> next(gener)
3
>>> next(gener)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```

Explain how there is no going back and you must re-create it by running my_gen() again to start over.
```
>>> next(gener)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>> next(gener)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>> gener = my_gen()
>>> next(gener)
1
```

Last, show how iteration is just like a list
>>> for item in my_gen():
...     print(item)
...
1
2
3

-------------------------------------------------------------------------------------------------------
3) Compare to lists

Show how this is not a list:
```
>>> gener = my_gen()
>>> gener[0]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'generator' object is not subscriptable
```

Show how this can become a list:
```
>>> list(my_gen())
[1, 2, 3]
```

Consider this generator that never stops yielding and ask what is different about these.
For Example, is it easy to make it a list? Try a few things on this generator.
```
>>> def forever():
...     while True:
...          yield 0
...
```

Here is how you can get part of it with islice from itertools:
```
>>> import itertools
<itertools.islice object at 0x7f7202157868>
>>> list(itertools.islice(forever(), 5))
[0, 0, 0, 0, 0]
```

And of course, you can iterate over it, but it never ends unless you break the loop


-------------------------------------------------------------------------------------------------------
4) Examples with Fibonacci

Have the students view and run the examples in the fibonacci directory

a) See fib_ex1.py for a non-generator approach to generating Fibonacci numbers.

b) See fib_ex2.py for a generator approach.

c) Bonus: To see how classes can be iterators and similar to generators, see fib_ex3.py
Generators are much shorter to write but classes can perform more complex tasks as they iterate.

-------------------------------------------------------------------------------------------------------
5) Complete the RC4 challenge in the challenge folder by completing the generator in challenge.py

